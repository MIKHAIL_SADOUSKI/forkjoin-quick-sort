package com.epam.mentoring.java.forkjoin;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.RecursiveAction;

public class QuickSortAction extends RecursiveAction {

    private List<Integer> sortElements;
    private int left;
    private int right;

    public QuickSortAction(List<Integer> sortElements) {
        this.sortElements = sortElements;
        left = 0;
        right = sortElements.size() - 1;
    }

    public QuickSortAction(List<Integer> sortElements, int left, int right) {
        this.sortElements = sortElements;
        this.left = left;
        this.right = right;
    }

    @Override
    protected void compute() {
        if (left < right) {
            int middle = partition(sortElements, left, right);
            invokeAll(new QuickSortAction(sortElements, left, middle),
                    new QuickSortAction(sortElements, middle + 1, right));
        }
    }

    private int partition(List<Integer> sortElements, int low, int high) {
        int middle = sortElements.get(low);
        int i = low - 1;
        int j = high + 1;

        while (true) {

            i++;
            j--;

            while (sortElements.get(i) < middle) {
                i++;
            }

            while (sortElements.get(j) > middle) {
                j--;
            }

            if (i >= j) {
                return j;
            }

            Collections.swap(sortElements, i, j);
        }
    }
}
