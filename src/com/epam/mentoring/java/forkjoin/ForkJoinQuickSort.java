package com.epam.mentoring.java.forkjoin;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

public class ForkJoinQuickSort {

    private static final int SIZE = 10000;

    public static void main(String[] args) {
        List<Integer> elementsToSort = new Random().ints(SIZE, -SIZE, SIZE).boxed().collect(Collectors.toList());

        QuickSortAction quickSort = new QuickSortAction(elementsToSort);

        ForkJoinPool pool = new ForkJoinPool();
        pool.invoke(quickSort);
        pool.shutdown();

        elementsToSort.forEach(System.out::println);
    }
}
